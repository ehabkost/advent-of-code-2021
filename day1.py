numbers = [int(l.strip()) for l in open('day1-input.txt').readlines()]
increases = [numbers[i] for i in range(1, len(numbers))
                        if numbers[i] > numbers[i-1]]
print(len(increases))

sums = [sum(numbers[i:i+3]) for i in range(len(numbers)-2)]
increases = [sums[i] for i in range(1, len(sums))
                     if sums[i] > sums[i-1]]
print(len(increases))